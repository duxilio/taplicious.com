

<?php
    include './php/Translator.php';

    if(isset($_GET['lang']) && $_GET['lang'] != ''){
        $lang_code = htmlentities($_GET['lang']);
    } else {
        $lang_code = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE'])[0];
    }

    $t = new Translator($lang_code);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Taplicious - Revolutionising the way you experience restaurants</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="Order and checkout on your phone, rate your food, get discounts, discover new food and much more. Be part of the revolution.">

    <meta name="author" content="Duxilio">
    <meta name="follow" content="index, follow">

    <meta property="og:site_name" content="Taplicious - Revolutionising the way you experience restaurants">
    <meta property="og:locale" content="en-GB">
    <meta property="og:type" content="website">
    <meta property="og:description" content="Order and checkout on your phone, rate your food, get discounts, discover new food and much more. Be part of the revolution.">
    <meta property="og:url" content="http://taplicious.com">
    <meta property="og:title" content="Taplicious - Revolutionising the way you experience restaurants">
    <meta property="og:image" content="http://taplicious.com/img/taplicious-og.png">
    <meta property="fb:app_id" content="">

    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@duxil_io">
    <meta name="twitter:url" content="http://taplicious.com">
    <meta name="twitter:title" content="Taplicious - Revolutionising the way you experience restaurants">
    <meta name="twitter:description" content="Order and checkout on your phone, rate your food, get discounts, discover new food and much more. Be part of the revolution.">
    <meta name="twitter:image" content="http://taplicious.com/img/taplicious-og.png">

    <link rel="apple-touch-icon" sizes="57x57" href="/img/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicons/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/img/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/img/favicons/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/img/favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/img/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/img/favicons/manifest.json">
    <link rel="mask-icon" href="/img/favicons/safari-pinned-tab.svg" color="#fa6441">
    <link rel="shortcut icon" href="/img/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#fa6441">
    <meta name="msapplication-TileImage" content="/img/favicons/mstile-144x144.png">
    <meta name="msapplication-config" content="/img/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" type="text/css" href="/css/main.min.css">
    <link rel="stylesheet" type="text/css" href="/fonts/ionicons/ionicons.css">

    <script src="https://use.typekit.net/oje1rfa.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
  </head>
  <body>

    
<div class="dialog" id="signup-dialog">
	<div class="v-align">
		<div class="v-align__cell">

			<div class="dialog__wrapper">
				<h4>Join the innovation</h4>
				<p>
					Sign up using your email below and be the first to know when we release in
					Amsterdam next year!
				</p>

				<form action="/php/form.php" class="js-signup-form form--minimal">
					<p><b class="js-signup-form__noti"></b></p>

					<label>Your email</label>
					<input type="email" name="email" placeholder="harvey@specter.com" required />
					<input class="js-form__submit" type="submit" value="Sign up">
				</form>

				<p>
					<a data-dialog-close="#signup-dialog" class="dialog__close">
						Close dialog
					</a>
				</p>
			</div>

		</div>
	</div>
</div>

<section class="v-align bg-img">
	<header class="header header--sticky">
		<div class="container">
			<div class="header__right">
				<small>Restaurant owner?</small>
				<small><a href="/" class="btn btn--push-left">Taplicious for Restaurants</a></small>
			</div>
		</div>
	</header>
	<section class="v-align__cell">
		<div class="container container--small">

			<div class="intro-content">
				<h1 class="logo">Taplicious</h1>
				<h2 class="h4">Revolutionise the restaurant experience</h2>
				<p>Order and checkout on your phone, rate your food, get discounts, discover new food and much more. Be part of the revolution.</p>

				<div><a class="btn btn--push-btm" data-dialog-open="#signup-dialog">
					Get started for free
				</a></div>
				<div><a class="btn btn--primary" href="https://www.youtube.com/watch?v=WRuU6TqxOs0">
					Watch the Pitch
				</a></div>
			</div>

			<div class="iphones">
				<img src="/img/devices-taplicious.png" alt="" />
			</div>

		</div>
	</section>

	<footer class="mastfoot mastfoot--sticky">
		<ul class="list list--inline">
			<li><a href="">Terms of Service</a></li>
			<li><a href="">Privacy Policy</a></li>
			<li><a href="mailto:support@taplicious.com">Support</a></li>
			<li><a href="http://duxilio.com">Copyright Duxilio 2015</a></li>
		</ul>
	</footer>
</section>

    <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js"></script>
    <script src="/js/app.js"></script>

    <script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-69976251-1', 'auto');
		ga('send', 'pageview');
	</script>

    <!-- Start of taplicious Zendesk Widget script -->
    <script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(c){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var o=this.createElement("script");n&&(this.domain=n),o.id="js-iframe-async",o.src=e,this.t=+new Date,this.zendeskHost=t,this.zEQueue=a,this.body.appendChild(o)},o.write('<body onload="document._l();">'),o.close()}("https://assets.zendesk.com/embeddable_framework/main.js","taplicious.zendesk.com");/*]]>*/</script>
    <!-- End of taplicious Zendesk Widget script -->

  </body>
</html>