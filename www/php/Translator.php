<?php

class Translator { 
	private $jsons_base_path = 'php/translations/';
	private $lang_json;
	public $jsons_paths = [];

	private function getJsonPaths(){
		return $this->json_paths;
	}

	private function setJsonPaths(array $paths){
		$this->json_paths = $paths;
	}

	public function __construct($lang_code){
		$this->setJsonPaths([
			'en-US' => 'en-GB',
			'en-GB' => 'en-GB',
			'nl-NL' => 'nl-NL',
			'default' => 'en-GB'
		]);

		$json_file_name = $this->getJsonFileName($lang_code);
		$json_path = $this->getJsonPath($json_file_name);

		$json_str = file_get_contents($json_path);
		$this->lang_json = json_decode($json_str, true);
	}

	private function getJsonFileName($lang_code){
		$jsons_paths = $this->getJsonPaths();

		if(isset($jsons_paths[$lang_code])){
			return $jsons_paths[$lang_code];
		}
		
		return $jsons_paths['default'];
	}

	private function getJsonPath($json_file_name){
		$jsons_base_path = $this->jsons_base_path;
		$jsons_paths = $this->getJsonPaths();

		$json_path = $jsons_base_path.$json_file_name.'.json';

		if(!file_exists($json_path)){
			//default lang
			$json_file_name = $jsons_paths['default'];
			return $jsons_base_path.$json_file_name.'.json';
		}
		
		return $json_path;
	}

	public function get($key){
		return $this->lang_json[$key];
	}

	public function show($key){
		echo $this->lang_json[$key];
	}
}