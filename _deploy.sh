DEPLOY_PATH="/usr/containers/taplicious"

echo "Deploying to $DEPLOY_PATH..."

rsync -av --exclude '.DS_Store' --exclude '.git' --exclude 'node_modules' --exclude '.sass-cache' ./www/ koen@mijn-roc.nl:$DEPLOY_PATH
ssh koen@mijn-roc.nl "chmod -R 755 $DEPLOY_PATH/img"
