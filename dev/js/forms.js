(function(){
	//init signup forms

	var formEls = document.getElementsByClassName('js-signup-form');

	for(var i = formEls.length-1; i >= 0; i--){
		var notiEl = formEls[i].getElementsByClassName('js-signup-form__noti')[0];

		new FormHelper(formEls[i], function(status){
			switch(status){
				case 'error':
					notiEl.innerHTML = 'An error occured, try again later. <br>Or <a href="mailto:support@taplicious.com">let us know about it.</a>';
					break;
				case 'success':
					notiEl.innerHTML = 'Done! We\'ll contact you as soon as we release!';
					break;
				case 'invalid':
					notiEl.innerHTML = 'Hmm, looks like you filled in some fields incorrectly';
					break;
			}
		});
	}
}());